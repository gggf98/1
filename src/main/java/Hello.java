public class Hello {
    public static String hello(){
        return "hello";
    }
    public static String helloAge(int age){
        if (age <= 0){
            throw new  IllegalArgumentException();
        }
//        return "Hello, " + age;
        return String.format("Hello, %d", age);
    }
}
